import json
from select import select
from tokenize import TokenInfo
from unicodedata import name
from flask import request, abort, g
from flask_restx import Resource, fields
from httplib2 import Http

from models.ticket import TicketModel
from schemas.ticket import TicketSchema

from server.instance import server

from multitenant.multi_db_management import get_tenant_session
from multitenant.caching import current_tenant

from models.user import User

oidc = server.oidc
ticket_ns = server.ticket_ns

ITEM_NOT_FOUND = "Ticket not found."

ticket_schema = TicketSchema()
ticket_list_schema = TicketSchema(many=True)

# Model required by flask_restx for expect
item = ticket_ns.model('Ticket', {
    'descricao': fields.String('Ticket description'),
    #'horas': fields.Integer(0),
})


class Ticket(Resource):
    @oidc.accept_token(require_token=True)#, scopes_required=['openid'])
    def get(self, id):
        ticket_data = TicketModel.find_by_id(id)
        if ticket_data:
            return ticket_schema.dump(ticket_data)
        return {'message': ITEM_NOT_FOUND}, 404

    @oidc.accept_token(require_token=True)
    def delete(self, id):
        ticekt_data = TicketModel.find_by_id(id)
        if ticekt_data:
            ticekt_data.delete_from_db()
            return '', 204
        return {'message': ITEM_NOT_FOUND}, 404

    @ticket_ns.expect(item)
    @oidc.accept_token(require_token=True)
    def put(self, id):
        ticket_data = TicketModel.find_by_id(id)
        ticket_json = request.get_json()

        if ticket_data:
            #ticket_data.horas = ticket_json['horas']
            ticket_data.descricao = ticket_json['descricao']
        else:
            ticket_data = ticket_schema.load(ticket_json)

        ticket_data.save_to_db()
        return ticket_schema.dump(ticket_data), 200


class TicketList(Resource):
    @ticket_ns.doc('Get all the Items')
    @oidc.accept_token(require_token=True)#, scopes_required=['SCOPE_teste']) #scopes_required=['openid'])
    def get(self):
        #tenant_name = request.headers.get('schema')
        tenant_session = get_tenant_session(current_tenant)
        #if not tenant_session:
        #    abort(404)
        #return ticket_list_schema.dump(tenant_session.query(TicketModel).all()), 200
        #return ticket_list_schema.dump(TicketModel.find_all()), 200
        
        tokenn = request.headers.get('Authorization').split(" ")[1]
        #url = "http://localhost:8080/auth/realms/saiyajin/protocol/openid-connect/userinfo"
        #headers = {'Authorization': 'Bearer '+tokenn, 'Content-Type': 'application/json'}
        #content = Http().request(uri=url, method='GET', headers=headers)
        #print(json.loads(str(content[1]).split("'")[1])["name"])
        #g.oidc_id_token = json.loads(str(content[1]).split("'")[1])["sub"]
        User().set_g_oidc_id_token(tokenn)
        print(g.oidc_id_token)
        #print(tenant_session.execute("select description from Ticket where id=1"))
        
        #print(requests.get(url, headers))
        print(oidc.user_getinfo(['sub','name','preferred_username','given_name','family_name','email','authorities'],tokenn))
        print(oidc.user_getfield("preferred_username",tokenn))
        return ticket_list_schema.dump(tenant_session.query(TicketModel).all()), 200

    @ticket_ns.expect(item)
    @ticket_ns.doc('Create a Ticket')
    @oidc.accept_token(require_token=True)
    def post(self):
        ticket_json = request.get_json()
        ticket_data = ticket_schema.load(ticket_json)

        ticket_data.save_to_db()

        return ticket_schema.dump(ticket_data), 201