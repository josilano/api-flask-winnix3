from db import db
from typing import List


class TicketModel(db.Model):
    __tablename__ = "ticket"

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(80), nullable=False, unique=False)
    #horas = db.Column(db.Float(precision=2), nullable=False)

    def __init__(self, descricao):
        self.description = descricao

    def __repr__(self):
        return f'TicketModel(descricao={self.description})'

    def json(self):
        return {'descricao': self.description}

    @classmethod
    def find_by_descricao(cls, description) -> "TicketModel":
        return cls.query.filter_by(description=description).first()

    @classmethod
    def find_by_id(cls, _id) -> "TicketModel":
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_all(cls) -> List["TicketModel"]:
        return cls.query.all()

    def save_to_db(self) -> None:
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self) -> None:
        db.session.delete(self)
        db.session.commit()