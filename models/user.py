import json

from httplib2 import Http
from flask import g


class User():
    def set_g_oidc_id_token(self, token) -> None:
        url = "http://localhost:8080/auth/realms/saiyajin/protocol/openid-connect/userinfo"
        headers = {'Authorization': 'Bearer '+token, 'Content-Type': 'application/json'}
        content = Http().request(uri=url, method='GET', headers=headers)
        print(json.loads(str(content[1]).split("'")[1])["name"])
        g.oidc_id_token = json.loads(str(content[1]).split("'")[1])["sub"]
        

