from flask import Flask, Blueprint
from flask_restx import Api
from flask_oidc import OpenIDConnect

from ma import ma
from db import db

from marshmallow import ValidationError

class Server():
    def __init__(self):
        self.app = Flask(__name__)
        self.bluePrint = Blueprint('api', __name__, url_prefix='/api')
        self.api = Api(self.bluePrint, doc='/doc', title='Api teste winnix Flask-Restx poc')
        self.app.register_blueprint(self.bluePrint)

        #self.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
        self.app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root@localhost/trac_projectchemtech?charset=utf8'
        self.app.config['SQLALCHEMY_BINDS'] = {}
        self.app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        self.app.config['PROPAGATE_EXCEPTIONS'] = True
        
        self.app.config.update({
            'SECRET_KEY': 'SomethingNotEntirelySecret',#os.urandom(42)
            'TESTING': True,
            'DEBUG': True,
            'OIDC_CLIENT_SECRETS': 'client_secrets.json',
            'OIDC_ID_TOKEN_COOKIE_SECURE': False,
            'OIDC_REQUIRE_VERIFIED_EMAIL': False,
            'OIDC_USER_INFO_ENABLED': True,
            'OIDC_OPENID_REALM': 'saiyajin',
            'OIDC_SCOPES': ['openid', 'email', 'profile', 'teste'],
            'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
            'OIDC_TOKEN_TYPE_HINT': 'access_token',
            'OIDC_RESOURCE_SERVER_ONLY':True
        })
        
        self.oidc = OpenIDConnect(self.app)

        self.ticket_ns = self.ticket_ns()

        super().__init__()

    def ticket_ns(self, ):
        return self.api.namespace(name='Tickets', description='operacoes com tickets', path='/')

    def run(self, ):
        self.app.run(
            port=5000,
            debug=True,
            host='0.0.0.0'
        )

server = Server()
