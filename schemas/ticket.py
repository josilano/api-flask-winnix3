from ma import ma
from models.ticket import TicketModel


class TicketSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = TicketModel
        load_instance = True