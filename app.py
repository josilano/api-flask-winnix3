from flask import Flask, Blueprint, jsonify, request
from flask_restx import Api
from ma import ma
from db import db

from resources.ticket import Ticket, TicketList, ticket_ns
from marshmallow import ValidationError

from server.instance import server
import logging
#from werkzeug.local import LocalStack
from multitenant.caching import tenant_stack
#tenant_stack = LocalStack()

api = server.api
app = server.app

logging.basicConfig(level=logging.DEBUG)

#@app.before_first_request
#def create_tables():
#    db.create_all()

@app.before_request
def push_tenant():
    tenant = request.headers.get('schema')
    print("schema no header "+tenant)
    tenant_stack.push(tenant)
    app.config['SQLALCHEMY_BINDS'][tenant] = f'mysql+pymysql://root:root@localhost/{tenant}?charset=utf8'
    app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql+pymysql://root:root@localhost/{tenant}?charset=utf8'
    db.session.connection(execution_options={"schema_translate_map": {"trac_projectchemtech": tenant, None: tenant}})

@app.teardown_request
def pop_tenant(self):
    tenant_stack.pop()

@api.errorhandler(ValidationError)
def handle_validation_error(error):
    return jsonify(error.messages), 400


api.add_resource(Ticket, '/tickets/<int:id>')
api.add_resource(TicketList, '/tickets')

if __name__ == '__main__':
    db.init_app(app)
    ma.init_app(app)
    server.run()