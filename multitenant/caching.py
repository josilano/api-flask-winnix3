from functools import wraps
#from flask_caching import FileSystemCache # .filesystem import get_filesystem_encoding
from werkzeug.local import LocalStack, LocalProxy

tenant_stack = LocalStack()

def get_tenant():
    top = tenant_stack.top
    if top is None:
        raise RuntimeError('No tenant found')
    return top

current_tenant = LocalProxy(get_tenant)
"""
cache = FileSystemCache()

def bindarguments(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        function_name = f.__name__
        if cache.has(function_name):
            result = cache.get(function_name)
        else:
            result = f(*args, **kwargs)
            cache.set(function_name, result)
        return result
    return wrapper
"""
"""
from functools import wraps
#from werkzeug.contrib.cache import SimpleCache
#from werkzeug import SimpleCache
#from flask_caching.backends.simplecache import SimpleCache# .backends.filesystemcache import FileSystemCache
from werkzeug.local import LocalStack
from werkzeug.middleware.dispatcher import DispatcherMiddleware
#from cachelib import SimpleCache

#cache = SimpleCache()
#cache = FileSystemCache()
cache = DispatcherMiddleware()

def simple_cache(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        function_name = f.__name__
        if cache.has(function_name):
            result = cache.get(function_name)
        else:
            result = f(*args, **kwargs)
            cache.set(function_name, result)
        return result
    return wrapper
"""
