from flask import current_app
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from werkzeug.local import LocalProxy
from db import db
#from multitenant.caching import bind_arguments

MYSQL_URI = 'mysql+pymysql://root:root@localhost/{}?charset=utf8'

_db_engines = {}
def _get_db_engines():
    if current_app.name not in _db_engines:
        engine = create_engine(current_app.config['SQLALCHEMY_DATABASE_URI'])
        _db_engines[current_app.name] = engine
    return _db_engines[current_app.name]

current_db = LocalProxy(_get_db_engines)
dbmy = current_db

def prepare_bind(tenant_name):
    if tenant_name not in current_app.config['SQLALCHEMY_BINDS']:
        current_app.config['SQLALCHEMY_BINDS'][tenant_name] = MYSQL_URI.format(tenant_name)
    return current_app.config['SQLALCHEMY_BINDS'][tenant_name]


def get_tenant_session(tenant_name):
    #if tenant_name not in get_known_tenants():
    #    return None
    prepare_bind(tenant_name)
    engine = db.get_engine(current_app, bind=tenant_name)
    session_maker = db.create_scoped_session()
    session_maker.configure(bind=engine)
    session = session_maker
    return session


""""
from multitenant.caching import simple_cache
from models.tenant import Tenant
from db import db
from flask import current_app

MYSQL_URI = 'mysql+pymysql://root:root@localhost/{}?charset=utf8'


@simple_cache
def get_known_tenants():
    tenants = 'trac_projectchemtech'#Tenant.query.all()
    return [i.name for i in tenants]


def prepare_bind(tenant_name):
    if tenant_name not in current_app.config['SQLALCHEMY_BINDS']:
        current_app.config['SQLALCHEMY_BINDS'][tenant_name] = MYSQL_URI.format(tenant_name)
    return current_app.config['SQLALCHEMY_BINDS'][tenant_name]


def get_tenant_session(tenant_name):
    if tenant_name not in get_known_tenants():
        return None
    prepare_bind(tenant_name)
    engine = db.get_engine(current_app, bind=tenant_name)
    session_maker = db.sessionmaker()
    session_maker.configure(bind=engine)
    session = session_maker()
    return session
"""
